/* window.rs
 *
 * Copyright 2024 Barricade Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use crate::config;
use adw::prelude::*;
use adw::subclass::prelude::*;
use epoch_converter::units;
use gettextrs::gettext;
use glib::clone;
use gtk::{gio, glib};
use std::time::Duration;
use zxcvbn::{self, feedback::Warning};

mod imp {
    use super::*;

    #[derive(Debug, gtk::CompositeTemplate, derivative::Derivative)]
    #[derivative(Default)]
    #[template(resource = "/io/gitlab/gregorni/Barricade/gtk/window.ui")]
    pub struct BarricadeWindow {
        #[template_child]
        pub password_entry: TemplateChild<gtk::PasswordEntry>,
        #[template_child]
        pub strength_bar: TemplateChild<gtk::LevelBar>,
        #[template_child]
        pub heading_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub status_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub warning_label: TemplateChild<gtk::Label>,
        #[derivative(Default(value = "gio::Settings::new(config::APP_ID)"))]
        pub settings: gio::Settings,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BarricadeWindow {
        const NAME: &'static str = "BarricadeWindow";
        type Type = super::BarricadeWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BarricadeWindow {
        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            self.settings
                .bind("window-height", obj.as_ref(), "default-height")
                .build();
            self.settings
                .bind("window-width", obj.as_ref(), "default-width")
                .build();
            self.settings
                .bind("window-is-maximized", obj.as_ref(), "maximized")
                .build();
        }
    }
    impl WidgetImpl for BarricadeWindow {}
    impl WindowImpl for BarricadeWindow {}
    impl ApplicationWindowImpl for BarricadeWindow {}
    impl AdwApplicationWindowImpl for BarricadeWindow {}
}

glib::wrapper! {
    pub struct BarricadeWindow(ObjectSubclass<imp::BarricadeWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl BarricadeWindow {
    pub fn new<P: IsA<gtk::Application>>(application: &P) -> Self {
        let win = glib::Object::builder::<BarricadeWindow>()
            .property("application", application)
            .build();

        win.connect_widgets();
        win.reset_widgets();
        win
    }

    fn connect_widgets(&self) {
        self.imp()
            .password_entry
            .connect_changed(clone!(@weak self as window => move |row| {
                if let Ok(estimate) = zxcvbn::zxcvbn(
                    &row.text(),
                    &[glib::user_name().to_str().expect("Couldn't get user's name")],
                ) {
                    window.set_score_widgets(&estimate);
                    window.set_feedback_widgets(&estimate);
                } else { // Also triggers if the input is empty
                    window.reset_widgets();
                }
            }));
    }

    fn set_feedback_widgets(&self, estimate: &zxcvbn::Entropy) {
        let imp = self.imp();

        if let Some(feedback) = estimate.feedback() {
            if let Some(warning) = feedback.warning() {
                imp.warning_label.set_label(
                    // Translators: Notice the space at the end!
                    &(gettext("Tip: ")
                        + &match warning {
                            Warning::ThisIsATop10Password
                            | Warning::ThisIsATop100Password
                            | Warning::ThisIsACommonPassword
                            | Warning::ThisIsSimilarToACommonlyUsedPassword => {
                                gettext("Use less common words")
                            }
                            Warning::DatesAreOftenEasyToGuess
                            | Warning::RecentYearsAreEasyToGuess => gettext("Avoid dates"),
                            Warning::StraightRowsOfKeysAreEasyToGuess
                            | Warning::ShortKeyboardPatternsAreEasyToGuess => {
                                gettext("Avoid rows of letters on your keyboard")
                            }
                            Warning::CommonNamesAndSurnamesAreEasyToGuess
                            | Warning::NamesAndSurnamesByThemselvesAreEasyToGuess => {
                                gettext("Avoid names")
                            }
                            Warning::RepeatsLikeAaaAreEasyToGuess
                            | Warning::RepeatsLikeAbcAbcAreOnlySlightlyHarderToGuess => {
                                gettext("Avoid repeats")
                            }
                            Warning::SequencesLikeAbcAreEasyToGuess => {
                                gettext("Avoid alphabetic sequences")
                            }
                            Warning::AWordByItselfIsEasyToGuess => {
                                gettext("Add more characters to your password")
                            }
                        }),
                );

                return;
            }
        }

        imp.warning_label.set_label("");
    }

    fn set_score_widgets(&self, estimate: &zxcvbn::Entropy) {
        let imp = self.imp();

        let indexing_score = estimate.score().clamp(2, 4) as usize - 2;

        imp.heading_label.set_label(
            &[
                gettext("Weak Password"),
                gettext("Moderate Password"),
                gettext("Strong Password"),
            ][indexing_score],
        );

        imp.status_label.set_label(&self.pretty_time_to_crack(
            Duration::from(estimate.crack_times().offline_slow_hashing_1e4_per_second()).as_secs(),
        ));

        imp.strength_bar.remove_css_class("weak");
        imp.strength_bar.remove_css_class("moderate");
        imp.strength_bar.remove_css_class("strong");
        imp.strength_bar
            .add_css_class(["weak", "moderate", "strong"][indexing_score]);

        let fraction_score = indexing_score as f64 + 1.0;
        self.animate_strength_bar(fraction_score);
    }

    fn animate_strength_bar(&self, position_fraction: f64) {
        let imp = self.imp();
        let current_fraction = imp.strength_bar.value();
        if current_fraction == position_fraction {
            return;
        }

        let downcast_bar = &<gtk::LevelBar as Clone>::clone(&imp.strength_bar)
            .downcast::<gtk::LevelBar>()
            .unwrap();

        let animation = adw::TimedAnimation::builder()
            .widget(downcast_bar)
            .value_from(current_fraction)
            .value_to(position_fraction)
            .duration(500)
            .target(&adw::PropertyAnimationTarget::new(downcast_bar, "value"))
            .easing(adw::Easing::EaseOutElastic)
            .build();

        if [0.0, 3.0].contains(&position_fraction) {
            animation.set_easing(adw::Easing::EaseOutBounce);
        }

        animation.play();
    }

    fn reset_widgets(&self) {
        let imp = self.imp();
        imp.heading_label.set_label(&gettext("Type a Password"));
        imp.status_label.set_label(&gettext(
            "Type a password to get a security rating and improvement suggestions",
        ));
        imp.warning_label.set_label("");

        self.animate_strength_bar(0.0);
    }

    fn pretty_time_to_crack(&self, time_in_secs: u64) -> String {
        let time = units::units(time_in_secs);

        if time.years > 100 {
            return gettext("This password couldn't be cracked for centuries");
        }

        if time.years > 1 {
            return gettext("This password could be cracked in several years");
        }

        if time.months > 1 {
            return gettext("This password could be cracked in a year");
        }

        if time.days > 1 {
            return gettext("This password could be cracked in a month");
        }

        if time.hours > 1 {
            return gettext("This password could be cracked in a day");
        }

        if time.minutes > 1 {
            return gettext("This password could be cracked in an hour");
        }

        return gettext("This password could be cracked in a minute");
    }
}
