# Barricade

Check the strength of a password

## Installation

The currently supported method of installation is via Flathub:

<a href='https://flathub.org/apps/details/io.gitlab.gregorni.Barricade'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

## Development

The easiest way to work on this project is by cloning it with GNOME Builder:

1. Install and open [GNOME Builder](https://flathub.org/apps/details/org.gnome.Builder)
2. Select "Clone Repository..."
3. Clone `https://gitlab.gnome.org/World/Barricade.git` (or your fork)
4. Run the project with the ▶ button at the top, or by pressing `Ctrl`+`Shift`+`Space`.

## Translation

[![Translation status](https://hosted.weblate.org/widget/Barricade/horizontal-auto.svg)](https://hosted.weblate.org/engage/Barricade/)

Barricade uses [Weblate](https://hosted.weblate.org/engage/Barricade/) for translation. Translations can be contributed there, or via a simple merge request. Don't forget to translate `"translator-credits"` with your credits!

## Code of Conduct

This project follows the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct)
